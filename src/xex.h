#ifndef XEX_H
#define XEX_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <unistd.h>
#define GETCWD getcwd
#define MKDIR(X) mkdir(X, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#ifdef __ORDER_LITTLE_ENDIAN__
#define BYTESWAP16 __builtin_bswap16
#define BYTESWAP32 __builtin_bswap32
#define BYTESWAP64 __builtin_bswap64
#else
#define BYTESWAP16
#define BYTESWAP32
#define BYTESWAP64
#endif

enum _xex_flags {
  TITLE_MODULE = 0x1,
  EXPORT_TO_TITLE = 0x2,
  SYSTEM_DEBUGGER = 0x4,
  DLL_MODULE = 0x8,
  MODULE_PATCH = 0x10,
  PATCH_FULL = 0x20,
  PATCH_DELTA = 0x40,
  USER_MODE = 0x80,
} xex_flags;

enum _optinal_header_id {
  RESOURCE_INFO = 0x2FF,
  BASE_FILE_FORMAT = 0x3FF,
  BASE_REFERNCE = 0x405,
  DELTA_PATCH_DESCRIPTOR = 0x5FF,
  BOUNDING_PATH = 0x80FF,
  DEVICE_ID = 0x8105,
  ORIGINAL_BASE_ADDRESS = 0x10001,
  ENTRY_POINT = 0x10100,
  IMAGE_BASE_ADDRESS = 0x10201,
  IMPORT_LIBRARIES = 0x103FF,
  CHECKSUM_TIMESTAMP = 0x18002,
  ENABLE_FOR_CALLCAP = 0x18102,
  ENABLE_FOR_FASTCAP = 0x18200,
  ORIGINAL_PE_NAME = 0x183FF,
  STATIC_LIBRARIES = 0x200FF,
  TLS_INFO = 0x20104,
  DEFAULT_STACK_SIZE = 0x20200,
  DEFAULT_FS_CACHE_SIZE = 0x20301,
  DEFAULT_HEAP_SIZE = 0x20401,
  PAGE_HEAP_SIZE_AND_FLAGS = 0x28002,
  SYSTEM_FLAGS = 0x30000,
  EXECUTION_ID = 0x40006,
  SERVICE_ID_LIST = 0x401FF,
  TITLE_WORKSPACE_SIZE = 0x40201,
  GAME_RATINGS = 0x40310,
  LAN_KEY = 0x40404,
  XBOX_360_LOGO = 0x405FF,
  MULTIDISC_MEDIA_IDS = 0x407FF,
  ADDITIONAL_TITLE_MEMORY = 0x40801,
  EXPORTS_BY_NAME = 0xE1042,
} optionalheader_id;

typedef struct _xex_header {
  uint32_t magic;
  uint32_t moduleFlags;
  uint32_t peDataOffset;
  uint32_t _reserved;
  uint32_t securityInfoOffset;
  uint32_t optionalHeaderCount;
} xex_header;

typedef struct _xex_opt_header {
  uint32_t id;
  uint32_t data;
} xex_opt_header;

int xexParseHeader(FILE *file, xex_header *xex);
void xexParseModuleFlags(uint32_t moduleFlags);
uint32_t xexParseOptionalHeader(xex_opt_header *opt_header);
int xexParseOptionalHeaders(FILE *file, xex_header *xex);

#endif