#include "xex.h"

int xexParseHeader(FILE *file, xex_header *xex) {
  fseek(file, 0, SEEK_SET);
  fread(xex, 24, 1, file);
  fseek(file, 0, SEEK_SET);

  xexParseModuleFlags(BYTESWAP32(xex->moduleFlags));

  if (BYTESWAP32(xex->magic) != 0x58455832) {
    printf("File is not XEX2 Format, exiting...\n");
    return -1;
  }

  printf("%-32s%8X\n", "PE Data Offset", BYTESWAP32(xex->peDataOffset));
  printf("%-32s%8X\n", "Security Info Offset",
         BYTESWAP32(xex->securityInfoOffset));
  printf("%-32s%8u\n", "Optional Header Count",
         BYTESWAP32(xex->optionalHeaderCount));

  xexParseOptionalHeaders(file, xex);

  return 0;
}

void xexParseModuleFlags(uint32_t moduleFlags) {
  printf("============= Module Flags =============\n");

  if (moduleFlags & TITLE_MODULE)
    printf("TITLE MODULE\n");
  if (moduleFlags & EXPORT_TO_TITLE)
    printf("EXPORT TO TITLE\n");
  if (moduleFlags & SYSTEM_DEBUGGER)
    printf("SYSTEM DEBUGGER\n");
  if (moduleFlags & DLL_MODULE)
    printf("DLL MODULE\n");
  if (moduleFlags & PATCH_FULL)
    printf("PATCH FULL\n");
  if (moduleFlags & PATCH_DELTA)
    printf("PATCH DELTA\n");
  if (moduleFlags & USER_MODE)
    printf("USER_MODE\n");

  printf("========================================\n");
}

uint32_t xexParseOptionalHeader(xex_opt_header *opt_header) {
  char *header_str;

  switch (BYTESWAP32(opt_header->id)) {
  case RESOURCE_INFO:
    header_str = "Resource Info";
    break;
  case BASE_FILE_FORMAT:
    header_str = "Base File Format";
    break;
  case BASE_REFERNCE:
    header_str = "Base Reference";
    break;
  case DELTA_PATCH_DESCRIPTOR:
    header_str = "Delta Patch Descriptor";
    break;
  case BOUNDING_PATH:
    header_str = "Bounding Path";
    break;
  case DEVICE_ID:
    header_str = "Device ID";
    break;
  case ORIGINAL_BASE_ADDRESS:
    header_str = "Original Base Address";
    break;
  case ENTRY_POINT:
    header_str = "Entry Point";
    break;
  case IMAGE_BASE_ADDRESS:
    header_str = "Image Base Address";
    break;
  case IMPORT_LIBRARIES:
    header_str = "Import Libraries";
    break;
  case CHECKSUM_TIMESTAMP:
    header_str = "Checksum Timestamp";
    break;
  case ENABLE_FOR_CALLCAP:
    header_str = "Enable for CallCap";
    break;
  case ENABLE_FOR_FASTCAP:
    header_str = "Enable for Fastcap";
    break;
  case ORIGINAL_PE_NAME:
    header_str = "Original PE Name";
    break;
  case STATIC_LIBRARIES:
    header_str = "Static Libraries";
    break;
  case TLS_INFO:
    header_str = "TLS Info";
    break;
  case DEFAULT_STACK_SIZE:
    header_str = "Default Stack Size";
    break;
  case DEFAULT_FS_CACHE_SIZE:
    header_str = "Defualt Filesystem Cache Size";
    break;
  case DEFAULT_HEAP_SIZE:
    header_str = "Default Heap Size";
    break;
  case PAGE_HEAP_SIZE_AND_FLAGS:
    header_str = "Page Heap Size and Flags";
    break;
  case SYSTEM_FLAGS:
    header_str = "System Flags";
    break;
  case EXECUTION_ID:
    header_str = "Execution ID";
    break;
  case SERVICE_ID_LIST:
    header_str = "Service ID List";
    break;
  case TITLE_WORKSPACE_SIZE:
    header_str = "Title Workspace Size";
    break;
  case GAME_RATINGS:
    header_str = "Game Ratings";
    break;
  case LAN_KEY:
    header_str = "LAN Key";
    break;
  case XBOX_360_LOGO:
    header_str = "Xbox 360 Logo";
    break;
  case MULTIDISC_MEDIA_IDS:
    header_str = "Multidisc Media IDs";
    break;
  case ADDITIONAL_TITLE_MEMORY:
    header_str = "Additional Title Memory";
    break;
  case EXPORTS_BY_NAME:
    header_str = "Exports by Name";
    break;

  default:
    header_str = "Unkown Optional Header ID";
    break;
  }

  printf("%-24s%-8X%8X ", header_str, BYTESWAP32(opt_header->id),
         BYTESWAP32(opt_header->data));

  switch (BYTESWAP32(opt_header->id) & 0xFF) {
  case 0x00:
    printf("Raw\n");
    return BYTESWAP32(opt_header->data);
  case 0x01: // TODO
    printf("Pointer\n");
    break;
  default: // TODO
    printf("Offset\n");
    break;
  }
}

int xexParseOptionalHeaders(FILE *file, xex_header *xex) {
  printf("=========== Optional Headers ===========\n");

  if (BYTESWAP32(xex->optionalHeaderCount) == 0) {
    printf("Optional Header Count is 0, Skipping...\n");
    return -1;
  }

  void *opt_headers =
      malloc(sizeof(xex_opt_header) * BYTESWAP32(xex->optionalHeaderCount));

  fseek(file, 24, SEEK_SET);
  fread(opt_headers,
        BYTESWAP32(xex->optionalHeaderCount) * sizeof(xex_opt_header), 1, file);

  xex_opt_header opt_header;
  for (int i = 0; i < BYTESWAP32(xex->optionalHeaderCount); i++) {
    memcpy(&opt_header, opt_headers + (i * sizeof(xex_opt_header)),
           sizeof(xex_opt_header));
    xexParseOptionalHeader(&opt_header);
  }

  printf("========================================\n");

  fseek(file, 0, SEEK_SET);

  free(opt_headers);

  return 0;
}