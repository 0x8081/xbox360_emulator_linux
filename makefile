CC			= gcc
CXX			= g++
CFLAGS		= -Os
CXXFLAGS	= -Os
SRCDIR		= src
CSRC		= $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.c))
CXXSRC		= $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.cpp))
#LIBS		=
#LDFLAGS		=
OBJS		= $(CSRC:%=%.o) $(CXXSRC:%=%.o)
TARGET		= test

build: $(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LDFLAGS)

%.c.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

%.cpp.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm $(TARGET) *.o